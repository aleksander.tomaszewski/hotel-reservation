package com.hotelreservationapi;

import com.hotelreservationapi.models.Reservation;
import com.hotelreservationapi.models.Room;
import com.hotelreservationapi.models.RoomType;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.UUID;

public interface Bookable {
    Reservation addReservation(String userName, int numberOfPeople, LocalDate startDate, LocalDate endDate);
    Reservation removeReservation(String userName);
    void listReservationsForRoom(UUID roomId);
    void listAllReservations();

    static Bookable newInstance() { return new Reception(
            Arrays.asList(
                    new Room(RoomType.BASIC),
                    new Room(RoomType.BASIC),
                    new Room(RoomType.SUIT),
                    new Room(RoomType.SUIT),
                    new Room(RoomType.PENTHOUSE)
            )
    ); }
}
