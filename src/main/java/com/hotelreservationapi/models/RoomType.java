package com.hotelreservationapi.models;

public enum RoomType {
    BASIC,
    SUIT,
    PENTHOUSE
}
