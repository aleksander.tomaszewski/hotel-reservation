package com.hotelreservationapi.models;

import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Value
@RequiredArgsConstructor
public class Room {
    UUID roomId = UUID.randomUUID();
    RoomType roomType;
    List<UUID> reservationsId = new ArrayList<>();

    public int getGuests() {
        switch (roomType) {
            case BASIC:
                return 4;
            case SUIT:
                return 6;
            case PENTHOUSE:
                return 8;
            default:
                return 0;
        }
    }

    public void addReservation(UUID reservationId) {
        reservationsId.add(reservationId);
    }
    public void removeReservation(UUID reservationId) {
        reservationsId.remove(reservationId);
    }
}
