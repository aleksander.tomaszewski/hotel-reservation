package com.hotelreservationapi.models;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.threeten.extra.LocalDateRange;

import java.time.LocalDate;
import java.util.UUID;

@Value
@RequiredArgsConstructor
public class Reservation {
    UUID reservationId = UUID.randomUUID();
    String userName;
    int numberOfPeople;
    LocalDate startDate;
    LocalDate endDate;
    UUID roomId;


    public LocalDateRange getReservationPeriod() {
        return LocalDateRange.of(startDate, endDate);
    }
    public void listReservationDetails() {
        String output = String.format(
                "User name: %s Number of people: %d From: %s, To: %s, Room ID: %s",
                getUserName(),
                getNumberOfPeople(),
                getStartDate(),
                getEndDate(),
                getRoomId().toString()
        );
        System.out.println(output);
    }
}
