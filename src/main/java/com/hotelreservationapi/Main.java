package com.hotelreservationapi;

import java.time.LocalDate;

public class Main {
    public static void main( String[] args ) {
        handleCommandLine(args);
    }

    private static void handleCommandLine(String[] args) {
        Validator validator = new Validator();

        String userName = args[0];
        Integer numberOfUsers = validator.validateInt(args[1]);
        LocalDate startDate = validator.validateDate(args[2], "Start");
        LocalDate endDate = validator.validateDate(args[3], "End");

        boolean isValidReservationPeriod = validator.validateReservationPeriod(startDate, endDate);

        if (isValidReservationPeriod && numberOfUsers != null) {
            Bookable reception = Bookable.newInstance();
            reception.addReservation(userName, numberOfUsers, startDate, endDate);
        }
    }
}
