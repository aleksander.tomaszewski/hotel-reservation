package com.hotelreservationapi;

import com.hotelreservationapi.models.Reservation;
import com.hotelreservationapi.models.Room;

import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.threeten.extra.LocalDateRange;

import java.time.LocalDate;
import java.util.*;

@Value
@RequiredArgsConstructor
public class Reception implements Bookable {
    List<Room> rooms;
    List<Reservation> reservations = new ArrayList<>();

    public Reservation addReservation(String userName, int numberOfPeople, LocalDate startDate, LocalDate endDate) {
        LocalDateRange period = LocalDateRange.of(startDate, endDate);
        Room freeRoom = getFreeRoomThatFits(numberOfPeople, period);

        if (freeRoom != null) {
            UUID roomThatFitId = freeRoom.getRoomId();
            Reservation newReservation = new Reservation(userName, numberOfPeople, startDate, endDate, roomThatFitId);

            reservations.add(newReservation);
            addReservationToTheRoom(roomThatFitId, newReservation);

            return newReservation;
        }

        return null;
    }

    public Reservation removeReservation(String userName) {
        Reservation reservationToRemove = getReservationBy(userName);
        if (reservationToRemove != null) {
            UUID roomId = reservationToRemove.getRoomId();

            reservations.remove(reservationToRemove);
            removeReservationFromTheRoom(roomId, reservationToRemove);
        }

        return reservationToRemove;
    }

    // Potentially poor performance
    public void listReservationsForRoom(UUID roomId) {
        findRoomById(roomId).ifPresent(room -> room.getReservationsId()
                .forEach(this::listReservationById));
    }

    public void listAllReservations() {
        if (reservations.isEmpty()) {
            System.out.println("There is no reservations");
        } else {
            for (Reservation reservation : reservations) {
                reservation.listReservationDetails();
            }
        }
    }

    public boolean isReservationsEmpty() {
        return reservations.isEmpty();
    }

    private boolean checkIfReservationPeriodIsAvailable(Room room, LocalDateRange period) {
        return room.getReservationsId()
                .stream()
                .noneMatch(reservationId -> isReservationOverlapsWithPeriod(reservationId, period));
    }

    private void addReservationToTheRoom(UUID roomId, Reservation reservation) {
        findRoomById(roomId)
                .ifPresent(room -> room.addReservation(reservation.getReservationId()));
    }

    private void removeReservationFromTheRoom(UUID roomId, Reservation reservation) {
        findRoomById(roomId)
                .ifPresent(room -> room.removeReservation(reservation.getReservationId()));
    }

    private Room getFreeRoomThatFits(int numberOfPeople, LocalDateRange period) {
        return rooms
                .stream()
                .filter(room -> numberOfPeople <= room.getGuests())
                .filter(room -> checkIfReservationPeriodIsAvailable(room, period))
                .min(Comparator.comparing(Room::getGuests))
                .orElse(null);
    }

    private Optional<Room> findRoomById(UUID roomId) {
        return rooms
                .stream()
                .filter(room -> roomId == room.getRoomId())
                .findFirst();
    }

    private Reservation getReservationBy(String userName) {
        return reservations
                .stream()
                .filter(reservation -> userName.equals(reservation.getUserName()))
                .findAny()
                .orElse(null);
    }

    private boolean isReservationOverlapsWithPeriod(UUID reservationId, LocalDateRange period) {
        return reservations
                .stream()
                .filter(reservation -> reservationId == reservation.getReservationId())
                .allMatch(reservation -> reservation.getReservationPeriod().overlaps(period));
    }

    private void listReservationById(UUID reservationId) {
        reservations
                .stream()
                .filter(reservation -> reservationId == reservation.getReservationId())
                .findFirst()
                .ifPresent(Reservation::listReservationDetails);
    }
}
