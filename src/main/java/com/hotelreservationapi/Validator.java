package com.hotelreservationapi;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class Validator {
    public Integer validateInt(String input) {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException exception) {
            System.err.println("Number of users \"" + input + "\" is not valid number.");
            return null;
        }
    }

    public LocalDate validateDate(String input, String forField) {
        try {
            return LocalDate.parse(input);
        } catch (DateTimeParseException exception) {
            System.err.println(forField + " date \"" + input + "\" is not a valid date.");
            return null;
        }
    }

    public boolean validateReservationPeriod(LocalDate startDate, LocalDate endDate) {
        if (endDate.isAfter(startDate)) {
            return true;
        } else {
            System.err.println("Reservation should be at least for 1 day.");
            return false;
        }
    }
}
