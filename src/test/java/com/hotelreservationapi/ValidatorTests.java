package com.hotelreservationapi;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

public class ValidatorTests {
    private final Validator validator = new Validator();

    @ParameterizedTest
    @ValueSource(strings = { "-1", "0", "01", "123" })
    public void test_givenDigitString_whenValidateInt_thanReturnInt(String input) {
        int result = validator.validateInt(input);
        assertThat(result).isEqualTo(Integer.parseInt(input));
    }

    @ParameterizedTest
    @ValueSource(strings = { "-0.25", "1.24", "example", " " })
    public void test_givenCharString_whenValidateInt_thanReturnNull(String input) {
        Integer result = validator.validateInt(input);
        assertThat(result).isNull();
    }

    @ParameterizedTest
    @ValueSource(strings = { "2022-12-01", "1999-01-12" })
    public void test_givenDateString_whenValidateDate_thanReturnLocalDate(String input) {
        LocalDate result = validator.validateDate(input, "");
        assertThat(result).isEqualTo(LocalDate.parse(input));
    }

    @ParameterizedTest
    @ValueSource(strings = { "2022-12-1", "1999-1-12", "123", " " })
    public void test_givenIncorrectDateString_whenValidateDate_thanReturnNull(String input) {
        LocalDate result = validator.validateDate(input, "");
        assertThat(result).isNull();
    }

    @ParameterizedTest
    @MethodSource("datesWhereStartDateIsBeforeEndDate")
    public void test_givenStartDateIsBeforeEndDate_whenValidateReservationPeriod_thanReturnTrue(LocalDate startDate, LocalDate endDate) {
        Boolean result = validator.validateReservationPeriod(startDate, endDate);
        assertThat(result).isTrue();
    }

    @ParameterizedTest
    @MethodSource("datesWhereStartDateIsAfterEndDate")
    public void test_givenStartDateIsAfterEndDate_whenValidateReservationPeriod_thanReturnFalse(LocalDate startDate, LocalDate endDate) {
        Boolean result = validator.validateReservationPeriod(startDate, endDate);
        assertThat(result).isFalse();
    }

    private static Stream<Arguments> datesWhereStartDateIsBeforeEndDate() {
        return Stream.of(
                Arguments.of(
                        LocalDate.of(2022, 12, 1),
                        LocalDate.of(2022, 12, 2)
                ),
                Arguments.of(
                        LocalDate.of(2022, 12, 1),
                        LocalDate.of(2022, 12, 29)
                ),
                Arguments.of(
                        LocalDate.of(2022, 12, 1),
                        LocalDate.of(2023, 11, 2)
                )
        );
    }

    private static Stream<Arguments> datesWhereStartDateIsAfterEndDate() {
        return Stream.of(
                Arguments.of(
                        LocalDate.of(2022, 12, 1),
                        LocalDate.of(2022, 11, 30)
                ),
                Arguments.of(
                        LocalDate.of(2021, 12, 1),
                        LocalDate.of(2020, 10, 29)
                )
        );
    }
}
