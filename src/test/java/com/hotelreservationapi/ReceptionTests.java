package com.hotelreservationapi;

import com.hotelreservationapi.models.Reservation;
import com.hotelreservationapi.models.Room;
import com.hotelreservationapi.models.RoomType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;

public class ReceptionTests {
    private Reception reception;

    private static final String USER_NAME = "Bob";
    private static final int NUMBER_OF_PEOPLE = 2;
    private static final LocalDate START_DATE = LocalDate.of(2022, 12, 1);
    private static final LocalDate END_DATE = LocalDate.of(2022, 12, 4);

    @BeforeEach
    void setUp() {
        reception = new Reception(
                Arrays.asList(
                new Room(RoomType.BASIC),
                new Room(RoomType.BASIC),
                new Room(RoomType.SUIT),
                new Room(RoomType.SUIT),
                new Room(RoomType.PENTHOUSE)
            )
        );
    }

    @ParameterizedTest
    @MethodSource("reservationsData")
    public void test_givenWithFreeRoomThatFit_whenAddReservation_thenReservationShouldBeAdded(
            String userName,
            int numberOfPeople,
            LocalDate startDate,
            LocalDate endDate
    ) {
        Reservation reservation = reception.addReservation(userName, numberOfPeople, startDate, endDate);

        assertThat(reservation)
                .returns(userName, from(Reservation::getUserName))
                .returns(numberOfPeople, from(Reservation::getNumberOfPeople))
                .returns(startDate, from(Reservation::getStartDate))
                .returns(endDate, from(Reservation::getEndDate));
    }

    @ParameterizedTest
    @MethodSource("reservationsData")
    public void test_givenNoRoomsThatFit_whenAddReservation_thanReservationShouldNotBeAdded(
            String userName,
            int numberOfPeople,
            LocalDate startDate,
            LocalDate endDate
    ) {
        reception = new Reception(new ArrayList<>());

        reception.addReservation(userName, numberOfPeople, startDate, endDate);

        assertThat(reception.isReservationsEmpty()).isTrue();
    }

    @Test
    public void test_givenRoomIsBookedAlreadyForCertainPeriod_whenAddReservation_thanReservationShouldNotBeAdded() {
        reception = new Reception(Collections.singletonList(new Room(RoomType.BASIC)));

        LocalDate newStartDate = LocalDate.of(2022, 12, 2);
        LocalDate newEndDate = LocalDate.of(2022, 12, 3);

        reception.addReservation(USER_NAME, NUMBER_OF_PEOPLE, START_DATE, END_DATE);

        assertThat(reception.getReservations().size()).isEqualTo(1);

        reception.addReservation(USER_NAME, NUMBER_OF_PEOPLE, newStartDate, newEndDate);

        assertThat(reception.getReservations().size()).isEqualTo(1);
    }

    @Test
    public void test_givenRequestedNumberOfUsersOverlapsMaximumGuestsAmountInRooms_whenAddReservation_thanReservationShouldNotBeAdded() {
        reception = new Reception(Collections.singletonList(new Room(RoomType.PENTHOUSE)));

        reception.addReservation(USER_NAME, 20, START_DATE, END_DATE);

        assertThat(reception.getReservations().isEmpty()).isTrue();
    }

    @Test
    public void test_givenWithAvailableBothRoomsThatFit_whenAddReservation_thanShouldAddReservationAndPickSmallerRoom() {
        Room expectedRoom = new Room(RoomType.BASIC);
        reception = new Reception(
                Arrays.asList(
                        new Room(RoomType.PENTHOUSE),
                        expectedRoom
                )
        );

        Reservation reservation = reception.addReservation(USER_NAME, NUMBER_OF_PEOPLE, START_DATE, END_DATE);

        assertThat(reservation.getRoomId()).isEqualTo(expectedRoom.getRoomId());
    }

    @ParameterizedTest
    @MethodSource("reservationsData")
    void test_givenUserNameForReservation_whenRemoveReservation_thanShouldRemoveReservation(
            String userName,
            int numberOfPeople,
            LocalDate startDate,
            LocalDate endDate
    ) {
        Reservation addedReservation = reception.addReservation(userName, numberOfPeople, startDate, endDate);

        assertThat(reception.isReservationsEmpty()).isFalse();

        Reservation removedReservation = reception.removeReservation(userName);

        assertThat(reception.isReservationsEmpty()).isTrue();
        assertThat(addedReservation).usingRecursiveComparison().isEqualTo(removedReservation);
    }

    @Test
    void test_givenWrongUserName_whenRemoveReservation_thanShouldNotRemoveReservation() {
        reception.addReservation(USER_NAME, NUMBER_OF_PEOPLE, START_DATE, END_DATE);

        assertThat(reception.isReservationsEmpty()).isFalse();

        reception.removeReservation("Joe");

        assertThat(reception.isReservationsEmpty()).isFalse();
    }

    private static Stream<Arguments> reservationsData() {
        return Stream.of(
                Arguments.of(
                        "Bob",
                        2,
                        LocalDate.of(2022, 12, 1),
                        LocalDate.of(2022, 12, 4)
                ),
                Arguments.of(
                        "Mike",
                        4,
                        LocalDate.of(2021, 11, 6),
                        LocalDate.of(2022, 1, 4)
                ),
                Arguments.of(
                        "John",
                        8,
                        LocalDate.of(2022, 1, 12),
                        LocalDate.of(2022, 2, 4)
                )
        );
    }
}
